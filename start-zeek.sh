#!/usr/bin/env bash

cp /usr/local/zeek/share/zeek/site/local.zeek.template /usr/local/zeek/share/zeek/site/local.zeek
printf '%s' "$ZEEK_CONFIG" >> /usr/local/zeek/share/zeek/site/local.zeek

envsubst '${CAPTURE_COUNTRY_CODE} ${CAPTURE_REGION_CODE} ${CAPTURE_CITY} ${CAPTURE_LATITUDE} ${CAPTURE_LONGITUDE}' < /usr/local/zeek/share/zeek/site/geoip-conn.zeek.template > /usr/local/zeek/share/zeek/site/geoip-conn.zeek

zeek -i "$INTERFACE" -C local

