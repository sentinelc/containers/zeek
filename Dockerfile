FROM zeek/zeek:6.1.1

RUN apt-get -q update \
 && apt-get install -q -y --no-install-recommends \
     gettext-base \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

COPY GeoLite2-City_20240510/GeoLite2-City.mmdb /usr/share/GeoIP/GeoLite2-City.mmdb
COPY geoip-conn.zeek.template /usr/local/zeek/share/zeek/site/geoip-conn.zeek.template

COPY local.zeek.template /usr/local/zeek/share/zeek/site/local.zeek.template

COPY start-zeek.sh /usr/local/zeek/bin/start-zeek.sh
RUN chmod 755 /usr/local/zeek/bin/start-zeek.sh

WORKDIR /logs

